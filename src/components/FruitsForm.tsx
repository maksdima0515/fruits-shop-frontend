import React, { FormEvent } from 'react';

class FruitsForm extends React.Component {
    private fruitName: React.RefObject<HTMLInputElement>;

    constructor (props?: any) {
        super(props);
        this.fruitName = React.createRef();
        this.send = this.send.bind(this);
    }
    
    send(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
        const data = { name: this.fruitName.current?.value }
        fetch("http://localhost:3000/", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (response.ok)
                alert(`Successfully added fruit: ${data.name}`);
            else
                alert(`Error: ${response.statusText}`);
        })
        .catch(error => {
            console.log(error);
        })
    }

    render(): React.ReactNode {
        return <form onSubmit={ this.send }>
        <label>Add a new fruit</label> <br />
        <input type="text" ref={ this.fruitName } autoComplete="off"/> <br />
        <input type="submit" value="Send"/>
        </form>
    }
}

export default FruitsForm;