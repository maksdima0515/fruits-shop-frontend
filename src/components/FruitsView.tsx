import React from 'react';

class FruitsView extends React.Component {
    constructor(props: any) {
        super(props);
        this.getAllFruits = this.getAllFruits.bind(this);
    }

    getAllFruits() {
        fetch("http://localhost:3000/", {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => {
            let result = "List of fruits:\n"

            data.forEach((item: any) => {
                const id = item.id;
                const name = item.name;
                result += `${id}: ${name}\n`;
            });
            alert(result)
        })
    }

    render(): React.ReactNode {
        return <div>
            <button onClick={ this.getAllFruits }>Get all fruits</button>
        </div>
    }
}

export default FruitsView;